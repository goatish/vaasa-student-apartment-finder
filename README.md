![Creative Commons License](https://i.creativecommons.org/l/by-sa/4.0/88x31.png) This work is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).

# README #

### Quickly find the right student apartment ###

This project was crerated to enable students to quickly find apartments suitable for their needs

Version 0.1

### How do I get set up? ###

This project uses the firebase-database. A dump can be found in `database/data.json`.
To locally serve the project for testing use `firebase serve`.
The project can be deployed from the commandline using `firebase deploy`.

### How can I help? ###

Please read [How to Contribute to Open Source](https://opensource.guide/how-to-contribute/).

### Who do I talk to? ###

info@raffaels-blog.de