'use strict';

/* Array of objects where each object is one student-home */
var allApartments = [];

const apartment_card = 
`<li><div class="apa-card-wide mdl-card mdl-shadow--2dp">
    <div class="mdl-card__title {color}">
      <h2 class="mdl-card__title-text">{name} • {type}</h2>
    </div>
    <div class="mdl-card__supporting-text">
      <p><i class="material-icons">euro_symbol</i><span class="prop-label">Price:</span><span class="prop-data">{price_range}</span>
      <p><i class="material-icons">location_on</i><span class="prop-label">Address:</span><span class="prop-data">{address}</span>
      <p><i class="material-icons">event_seat</i><span class="prop-label">Furnished:</span><span class="prop-data">{furnished}</span>
      <p><i class="material-icons">wifi</i><span class="prop-label">Internet:</span><span class="prop-data">{internet}</span>
      <p><i class="material-icons">flash_on</i><span class="prop-label">Electricity:</span><span class="prop-data">{electricity}</span>
      <p><i class="material-icons">refresh</i><span class="prop-label">Renovated in:</span><span class="prop-data">{yearRenovated}</span>
      <!-- <p><i class="material-icons">create</i><span class="prop-label">Note:</span><span class="prop-data">{note}</span> -->
    </div>
    <div class="mdl-card__menu">
      <button class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect">
        <i class="material-icons">share</i>
      </button>
      <button class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect">
        <i class="material-icons">star_border</i>
      </button>
    </div>
</div></li>`;

/* Represents the state of the checkboxes. */
var myHouse = {
    Shared: true,
    Family: true,
    Single: true
};

/* Called when a checkbox is clicked. Updates the state */
function toggleCheckbox(checkbox) {
    myHouse[checkbox.value] = !(myHouse[checkbox.value]);
    renderState();
}

/* Renders the state of myHouse to results list. */
function renderState() {
    var apToDisplay = [];

    /* Select apartments that meet the current state */
    Object.keys(myHouse).forEach(function(apartment_type) {
        if(myHouse[apartment_type]) {
            apToDisplay = apToDisplay.concat(allApartments.filter(function(apartments) {
                return apartments['type'] == apartment_type;
            }));
        }
    });

    const colors = {
        'Single': 'indigo300',
        'Shared': 'indigo500',
        'Family': 'indigo700'
    };

    var results_list = document.getElementById('results_list');
    results_list.innerHTML = "";

    /* Append each apartment as list-element to the results list. */
    apToDisplay.forEach(function(apartment){
        var address = "<a href='https://www.google.fi/maps/place/" + apartment.address.split(" ").join("+") + "/@63.1036261,21.6253675,14z' target='_blank'>" + apartment.address + "</a>";
        var html = apartment_card
                    .replace("{color}", colors[apartment['type']])
                    .replace("{name}", apartment['name'])
                    .replace("{type}", apartment['type'])
                    .replace("{price_range}", apartment['priceLowest'] + ' - ' + apartment['priceHighest'] + ' €')
                    .replace("{address}", address)
                    .replace("{furnished}", apartment['furnished'] || '(no information)')
                    .replace("{internet}", apartment['internet'])
                    .replace("{electricity}", apartment['electricity'])
                    .replace("{yearRenovated}", apartment['yearRenovated'] || '(no information)');
                    //.replace("{note}", apartment['note']);
        var div = document.createElement('div');
        div.innerHTML = html;
        results_list.appendChild(div);
    });
}

document.addEventListener('DOMContentLoaded', function () {
	/* Establish connection to firebase database */
    try {
        var app_1 = firebase.app();
        var features = ['auth', 'database', 'messaging', 'storage'].filter(function (feature) { return typeof app_1[feature] === 'function'; });
        console.log("Firebase SDK loaded with " + features.join(', '));
    }
    catch (e) {
        console.error(e);
        document.getElementById('load').innerHTML = 'Error loading the Firebase SDK, check the console.';
    }
    /* Request object with all apartments from the database. */
    firebase.database().ref('apartments').once('value').then(function (snapshot) {
    	/* Append each apartment to the list. */
        snapshot.forEach(function (house) {
            allApartments.push(house.toJSON());
        });
    }).then(function(){
        myHouse["Single"] = document.getElementById("checkbox-1").checked;
        myHouse["Shared"] = document.getElementById("checkbox-2").checked;
        myHouse["Family"] = document.getElementById("checkbox-3").checked;
        
        renderState();
    });

    document.getElementById("map-tab-button").addEventListener("click", function() {
        var fixed_tab_map = document.getElementById("fixed-tab-map");
        fixed_tab_map.innerHTML = `<iframe class="google-map" src="https://www.google.com/maps/d/embed?mid=15c5BhIIKJpJSQw_I___kIE0U8Es&ll=63.11%2C21.611&z=13" allowfullscreen></iframe>`;
    });
});